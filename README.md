# gtknotes

gtknotes is a series of demos of [GtkD](https://gtkd.org/). I have made this repo for my personal use to take notes for various Gtk(D) related topics because I believe documentations on this wrapper is scarce. This is not meant to be a comprehensive "Learn GtkD by Example" tutorial but quality-improvment PRs are very welcome.

### What is GtkD?

GtkD is an object oriented GTK+ wrapper for D programming language. It wraps almost all of the GTK+ stack and provides a beutiful interface to D. GtkD is a very feature-rich API designed to make GTK+ programming easy.


### How do I get started with GtkD?

Set up your IDE first. Then create a D project with dub via `dub init` command. [Add GtkD dependency](https://code.dlang.org/packages/gtk-d) to the dub.json file. 

```json
{
	"name": "gtkdnotes",
	"authors": [
		"user1101"
	],
	"description": "A minimal D application.",
	"copyright": "Copyright © 2018, user1101",
	"license": "MIT",
	"dependencies": {
		"gtk-d:gtkd": "~>3.9.0"
	}
}
````

Make sure your system already has the required dependencies of gtkd listed in website linked above.

Example `app.d`:

```d
import gio.Application : GioApplication = Application;
import gtk.Application;
import gtk.ApplicationWindow;

int main(string[] args) {
    auto application = new Application(
                            "org.gtkd.demo.helloworld", 
                            GApplicationFlags.FLAGS_NONE
                       );        
    application.addOnActivate(delegate void(GioApplication app) {
        auto appWindow = new ApplicationWindow(application);
        appWindow.setSizeRequest(200, 200);
        appWindow.showAll();
    });
    return application.run(args);
}
```

### Few things you should know:

* The examples in this project is purely for demonstration purposes. Which means most of these codes are not optimized for actual use, not designed for optimal maintainability. Your suggestions and PRs are very welcome.

* This repo demonstrates a subset of [pygtk tutorial](https://python-gtk-3-tutorial.readthedocs.io/en/latest/index.html). If you feel confused you can refer to that tutorial.

* The pygtk tutorial is far from a cmprehensive documentation. I suggest checking out [Vala's documentation](https://valadoc.org/gtk+-3.0/Gtk.html) instead.