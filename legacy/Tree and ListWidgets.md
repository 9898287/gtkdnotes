## Note: Majority of this article has been copied from the recommended reading material mentioned at the end of this page.

## TODO: Major revamp

# Gtk TreeStore and ListStore

 To create a tree or list in GTK+, use the Gtk TreeModel interface in conjunction with the Gtk TreeView widget. This widget is designed around a Model/View/Controller design and consists of four major parts:

- The tree view widget (GtkTreeView)

- The view column (GtkTreeViewColumn)

- The cell renderers (GtkCellRenderer etc.)

- The model interface (GtkTreeModel)

The **View** is composed of the first three objects, while the last is the **Model**. One of the prime benefits of the MVC (Model-View-Controller) design is that multiple views can be created of a single model. For example, a model mapping the file system could be created for a file manager. Many views could be created to display various parts of the file system, but only one copy need be kept in memory.

The purpose of the `CellRenderer`s is to provide extensibility to the widget and to allow multiple ways of rendering the same type of data. For example, consider how to render a boolean variable. Should it render it as a string of "True" or "False", "On" or "Off", or should it be rendered as a `Checkbox`? 

---

<details><summary><b>In-depth</b></summary>

<p><h3>GtkTreeModels for Data Storage: GtkListStore and GtkTreeStore</h3>

It is important to realise what GtkTreeModel is and what it is not. **GtkTreeModel is basically just an `interface`** to the data store, meaning that it is a standardised set of functions that allows a GtkTreeView widget (and the application programmer) to query certain characteristics of a data store, for example how many rows there are, which rows have children, and how many children a particular row has. It also provides functions to retrieve data from the data store, and tell the tree view what type of data is stored in the model. Every data store must implement the GtkTreeModel interface and provide these functions, which you can use by casting a store to a tree model. GtkTreeModel itself only provides a way to query a data store's characteristics and to retrieve existing data, it **does not provide a way to remove or add rows to the store or put data into the store. This is done using the specific store's functions.** 

Gtk+ comes with two built-in data stores (models): Gtk `ListStore` and Gtk `TreeStore`. As the names imply, Gtk `ListStore` is used for simple lists of data items where items have **no hierarchical parent-child relationships**, and Gtk `TreeStore` is used for tree-like data structures, **where items can have parent-child relationships**. A list of files in a directory would be an example of a simple list structure, whereas a directory tree is an example for a tree structure. A list is basically just a special case of a tree with none of the items having any children, so one could use a tree store to maintain a simple list of items as well. The only reason Gtk `ListStore` exists is in order to provide an easier interface that does not need to cater for child-parent relationships, and because a simple list model **can be optimised** for the special case where no children exist, which makes it faster and **more efficient**.

GtkListStore and GtkTreeStore should cater for most types of data an application developer might want to display in a GtkTreeView. However, it should be noted that GtkListStore and GtkTreeStore have been designed with flexibility in mind. **If you plan to store a lot of data, or have a large number of rows, you should consider implementing your own custom model** that stores and manipulates data your own way and implements the GtkTreeModel interface. This will not only be **more efficient**, but probably also lead to **saner code** in the long run, and give you **more control** over your data.

Tree model implementations like GtkListStore and GtkTreeStore will take care of the view side for you once you have configured the GtkTreeView to display what you want. If you change data in the store, the model will notify the tree view and your data display will be updated. If you add or remove rows, the model will also notify the store, and your row will appear in or disappear from the view as well.

A model (or data store) has model columns and rows. While a tree view will display each row in the model as a row in the view, **the model's columns are not to be confused with a view's columns**. A model column represents a certain data field of an item that has a fixed data type. You need to know what kind of data you want to store when you create a list store or a tree store, as you can not add new fields later on.

For example, we might want to display a list of files. We would create a list store with two fields: a field that stores the filename (i.e. a string) and a field that stores the file size (i.e. an unsigned integer). The filename would be stored in column 0 of the model, and the file size would be stored in column 1 of the model. For each file we would add a row to the list store, and set the row's fields to the filename and the file size. 

The GLib type system (GType) is used to indicate what type of data is stored in a model column. These are the most commonly used types:

- GType.BOOLEAN

- GType.INT, GType.UINT

- GType.LONG, GType.ULONG, GType.INT64, GType.UINT64 (these are not supported in early gtk+-2.0.x versions)

- GType.FLOAT, GType.DOUBLE

- GType.POINTER - stores a pointer value (may not copy any data into the store, just stores the pointer value)

- GType.STRING - stores a string in the store (may make a copy of the original string)

- `gdkpixbuf.Pixbuf.Pixbuf.getType()` - stores a GdkPixbuf in the store (ass seen [here](https://github.com/gtkd-developers/GtkD/blob/master/demos/gtkD/TestWindow/TestTreeView1.d#L143)) 

It will usually suffice to know the above types, so you can tell a list store or tree store what kind of data you want to store. Advanced users can derive their own types from the fundamental GLib types. For simple structures you could register a new boxed type for example, but that is usually not necessary. 

Here is an example of how to create a list store: 

```d
import gtk.ListStore : ListStore;
import gtkc.gobjecttypes : GType;

auto countryModel = new ListStore([GType.STRING, GType.STRING]);
```

<h3>Refering to Rows: GtkTreeIter, GtkTreePath, GtkTreeRowReference</h3>

There are different ways to refer to a specific row. The two you will have 
to deal with are GtkTreeIter and GtkTreePath. 

<b>GtkTreePath</b> (Describing a row "geographically")

A GtkTreePath is a comparatively straight-forward way to describe the logical position of a row in the model. As a GtkTreeView always displays all rows in a model, a tree path always describes the same row in both model and view.

To clarify this, a tree path of "3:9:4:1" would basically mean in human language **(attention - this is not what it really means!)** something along the lines of: go to the 3rd top-level row. Now go to the 9th child of that row. Proceed to the 4th child of the previous row. Then continue to the 1st child of that. Now you are at the row this tree path describes. This is not what it means for Gtk+ though. While humans start counting at 1, computers usually start counting at 0. So the real meaning of the tree path "3:9:4:1" is: Go to the **4th** top-level row. Then go to the **10th** child of that row. Pick the **5th** child of that row. Then proceed to the **2nd** child of the previous row. Now you are at the row this tree path describes.

The implication of this way of referring to rows is as follows: **if you insert or delete rows in the middle or if the rows are resorted, a tree path might suddenly refer to a completely different row than it referred to before the insertion/deletion/resorting.** This is important to keep in mind. This effect becomes apparent if you imagine what would happen if we were to delete the row entitled 'funny clips' from the tree in the above picture. The row 'movie trailers' would suddenly be the first and only child of 'clips', and be described by the tree path that formerly belonged to 'funny clips', i.e. "1:0:0".

You can get a new GtkTreePath from a path in string form using `new gtk.TreePath.TreePath(string path)`, and you can convert a given GtkTreePath into its string notation with `gtk.TreePath.TreePath.toString()`. Usually you will rarely have to handle the string notation, it is described here merely to demonstrate the concept of tree paths. 

Instead of the string notation, GtkTreePath uses an integer array internally. You can get the depth (i.e. the nesting level) of a tree path with `gtk.TreePath.TreePath.getDepth()`. A depth of 0 is the imaginary invisible root node of the tree view and model. A depth of 1 means that the tree path describes a top-level row. As lists are just trees without child nodes, all rows in a list always have tree paths of depth 1. `gtk.TreePath.TreePath.getIndices()` returns the internal integer array of a tree path. You will rarely need to operate with those either. 

If you operate with tree paths, you are most likely to use a given tree path, and use functions like `gtk.TreePath.TreePath.up()`, `gtk.TreePath.TreePath.down()`, `gtk.TreePath.TreePath.next()`, `gtk.TreePath.TreePath.prev()`, `gtk.TreePath.TreePath.isAncestor()`, or `gtk.TreePath.TreePath.isDescendant()`. Note that this way you can construct and operate on tree paths that refer to **rows that do not exist** in model or view. The only way to check whether a path is valid for a specific model (i.e. the row described by the path exists) is to convert the path into an iter.

GtkTreePath is an opaque structure, with its details hidden from the compiler. If you need to make a copy of a tree path, use `gtk.TreePath.TreePath.copy()`. 

<b>GtkTreeIter</b> (Referring to a row in model-speak)

Another way to refer to a row in a list or tree is GtkTreeIter. A tree iter is just a structure that contains a couple of pointers that mean something to the model you are using. Tree iters are used internally by models, and they often contain a direct pointer to the internal data of the row in question. You should never look at the content of a tree iter and you must not modify it directly either. 

All tree models (and therefore also GtkListStore and GtkTreeStore) must support the GtkTreeModel functions that operate on tree iters (e.g. get the tree iter for the first child of the row specified by a given tree iter, get the first row in the list/tree, get the n-th child of a given iter etc.). Some of these functions are:

- `abstract int gtk.TreeModelIF.TreeModelIF.getIterFirst()` - sets the given iter to the first top-level item in tree.

- `abstract bool gtk.TreeModelIF.TreeModelIF.iterNext()` - sets the given iter to the next item at the current level in a tree.

- `abstract bool gtk.TreeModelIF.TreeModelIF.iterChildren()` - sets the first given iter to the first child of the row referenced by the second iter.

- `abstract int gtk.TreeModelIF.TreeModelIF.iterNChildren()` - returns the number of children the row referenced by the provided iter has. If you pass NULL instead of a pointer to an iter structure, this function will return the number of top-level rows.

- `abstract bool gtk.TreeModelIF.TreeModelIF.iterNthChild()` - sets the first iter to the n-th child of the row referenced by the second iter. If you pass NULL instead of a pointer to an iter structure as the second iter, you can get the first iter set to the n-th row of a list.

- `abstract bool gtk.TreeModelIF.TreeModelIF.iterParent()` - sets the first iter to the parent of the row referenced by the second iter of a tree.

Almost all of those bool functions return true if the requested operation succeeded, and return false otherwise. There are more functions that operate on iters. Check out the GtkTreeModel API reference for details.

<b>GtkTreeRowReference</b> (Keeping track of rows even when the model changes)

A GtkTreeRowReference is basically an object that takes a tree path, and watches a model for changes. If anything changes, like rows getting inserted or removed, or rows getting re-ordered, the tree row reference object will keep the given tree path up to date, so that it always points to the same row as before. In case the given row is removed, the tree row reference will become invalid. 

A new tree row reference can be created with `new gtk.TreeRowReference.TreeRowReference(TreeModelIF model, TreePath path)` or `new gtk.TreeRowReference.TreeRowReference(ObjectG proxy, TreeModelIF model, TreePath path)`, given a model and a tree path. After that, the tree row reference will keep updating the path whenever the model changes. The current tree path of the row originally referred to when the tree row reference was created can be retrieved with `TreePath gtk.TreeRowReference.TreeRowReference.getPath()`. If the row has been deleted, NULL will be returned instead of a tree path. The tree path returned is a copy, so it will need to be freed with `gtk.TreeRowReference.TreeRowReference.free()` when it is no longer needed. 

You can check whether the row referenced still exists with `gtk.TreeRowReference.TreeRowReference.valid()`, and `free` it with when no longer needed.

Internally, the tree row reference connects to the tree model's "row-inserted", "row-deleted", and "rows-reordered" signals and updates its internal tree path whenever something happened to the model that affects the position of the referenced row.

Note that using tree row references entails a small overhead. This is hardly significant for 99.9% of all applications out there, but when you have multiple thousands of rows and/or row references, this might be something to keep in mind (because whenever rows are inserted, removed, or reordered, a signal will be sent out and processed for each row reference). 

If you have read the tutorial only up to here so far, it is hard to explain really what tree row references are good for. An example where tree row references come in handy can be found further below in the section on removing multiple rows in one go.

In practice, a programmer can either use tree row references to keep track of rows over time, or store tree iters directly (if, and only if, the model has persistent iters). Both GtkListStore and GtkTreeStore have persistent iters, so storing iters is possible. However, **using tree row references is definitively the proper way** to do things, even though it comes with some overhead that might impact performance in case of trees that have a very large number of rows (in that case it might be preferable to write a custom model anyway though). Especially beginners might find it easier to handle and store tree row references than iters, because tree row references are handled by pointer value, which you can easily add to a GList or pointer array, while it is easy to store tree iters in a wrong way. 

</p> 
</details>

---

## Adding rows to a store

<h3>Adding rows to a ListStore</h3>

```d
void main(string[] args) {
    import gtk.Main : Main;
    import gtk.MainWindow : MainWindow;

    Main.init(args);
    scope (success)
        Main.run();

    auto mw = new MainWindow("GTK MVC Demo");
    scope (success)
        mw.showAll();
    mw.setDefaultSize(320, 0);


    ///////////////////////////////// Read this section /////////////////////////////////
    // Create a country liststore with two string columns
    import gtk.ListStore : ListStore;
    import gtkc.gobjecttypes : GType;
    import gtk.TreeIter : TreeIter;

    enum COLUMNS {
        PLACE,
        PLAYER
    }

    auto listStore = new ListStore([GType.UINT, GType.STRING]);

    // now let's add 3 empty rows
    auto iter = listStore.createIter();
    //TreeIter can also be created simply with:
    //auto iter = new TreeIter();
    //listStore.append(iter);
    
    listStore.append(iter); // iter now points to the new row
    listStore.append(iter); // iter now points to the new row
    listStore.append(iter); // iter now points to the new row

    // add another row and set values to the rows this time
    listStore.append(iter);
    listStore.setValue(iter, COLUMNS.PLACE, 1);
    listStore.setValue(iter, COLUMNS.PLAYER, "Anthony Ervin");
    ///////////////////////////////// Read this section /////////////////////////////////

    
    // View and Controller
    import gtk.TreeView : TreeView;
    import gtk.TreeViewColumn : TreeViewColumn;
    import gtk.CellRendererText : CellRendererText;

    auto tv = new TreeView();
    tv.appendColumn(new TreeViewColumn("Place", new CellRendererText(), "text", COLUMNS.PLACE));
    tv.appendColumn(new TreeViewColumn("Player", new CellRendererText(), "text", COLUMNS.PLAYER));
    tv.setModel(listStore);

    import gtk.Box : Box;
    import gtkc.gtktypes : GtkOrientation;

    auto vbox = new Box(GtkOrientation.VERTICAL, 5);
    scope (success)
        mw.add(vbox);

    vbox.packStart(tv, true, true, 0);
}
```

![](https://i.imgur.com/ikbFlgI.png)

* Note that thre rows are automatically populated with each of `GType`s default value, like `UINT(0)` and `STRING("")`.

<h3>Adding rows to a TreeStore</h3>

No dramatical changes there. If you want to add a child to a row, feed the iterator of the parent row to the setValue function. This will return a new iterator that iterates through the child row.

<h4>LocationTreeStore.d</h4>

```d
module LocationTreeStore;

private import gtk.TreeStore;
private import gtk.TreeIter;
private import gtkc.gobjecttypes;

class LocationTreeStore : TreeStore
{
    this()
    {
        super([GType.STRING, GType.UINT]);
    }
   
    // Adds a location and returns the TreeIter of the item added.
    public TreeIter addLocation(in string name, in uint population)
    {
       
        TreeIter iter = createIter();
        setValue(iter, 0, name);
        setValue(iter, 1, population);
        return iter;
    }
   
    // Adds a child location to the specified parent TreeIter.
    public TreeIter addChildLocation(TreeIter parent,
            in string name, in uint population)
    {
        TreeIter child = TreeStore.createIter(parent);
        setValue(child, 0, name);
        setValue(child, 1, population);
        return child;
    }
}
```

`TreeIter child = TreeStore.createIter(parent);`

This is the main difference of the tree store. When we create a new row, we pass a parent TreeIter so the new row is a child of that parent.


<h4>LocationTreeView.d</h4>

```d
module LocationTreeView;

private import gtk.TreeView;
private import gtk.TreeViewColumn;
private import gtk.TreeStore;
private import gtk.CellRendererText;
private import gtk.ListStore;

class LocationTreeView : TreeView
{
    private TreeViewColumn countryColumn;
    private TreeViewColumn capitalColumn;
   
    this(TreeStore store)
    {       
        // Add Country Column
        countryColumn = new TreeViewColumn(
            "Location", new CellRendererText(), "text", 0);
        appendColumn(countryColumn);
       
        // Add Capital Column
        capitalColumn = new TreeViewColumn(
            "Population", new CellRendererText(), "text", 1);
        appendColumn(capitalColumn);
       
        setModel(store);
    }
}
```

No major changes on the TreeView.

<h4>main.d</h4>

```d
import gtk.MainWindow;
import gtk.Box;
import gtk.Main;

import LocationTreeStore;
import LocationTreeView;

void main(string[] args)
{
    Main.init(args);
    MainWindow win = new MainWindow("TreeStore Example");
    win.setDefaultSize(250, 280);
 
    Box box = new Box(Orientation.VERTICAL, 0);
   
    auto store = new LocationTreeStore();
   
    // Add Asia
    auto asia = store.addLocation("Asia", 4_165_440_000);
    auto japan = store.addChildLocation(asia, "Japan", 128_056_026);
    store.addChildLocation(japan, "Tokyo", 35_682_000);
   
    // Add Europe
    auto europe = store.addLocation("Europe", 742_452_000);
    auto finland = store.addChildLocation(europe, "Finland", 5_180_000);
    store.addChildLocation(finland, "Helsinki", 1_361_000);
   
    // Add North America
    auto northAmerica = store.addLocation("North America", 528_700_000);
    auto usa = store.addChildLocation(northAmerica,
            "United States", 313_900_000);
    store.addChildLocation(usa, "New York", 8_245_000);
   
    auto locationTreeView = new LocationTreeView(store);
    box.packStart(locationTreeView, true, true, 0);
 
    win.add(box);
    win.showAll();
    Main.run();
}
```

* Notice how we keep a reference to the added node so we can add children. You can of course find the children another way, such as iterating through them, or sorting and searching. But for the simplicity of this example, we simply store the parent in a variable and pass it to addChildLocation.

## Speed Issues when Adding a Lot of Rows

A common scenario is that a model needs to be filled with a lot of rows at some point, either at start-up, or when some file is opened. An equally common scenario is that this takes an awfully long time even on powerful machines once the model contains more than a couple of thousand rows, with an exponentially decreasing rate of insertion. As already pointed out above, writing a custom model might be the best thing to do in this case. Nevertheless, there are some things you can do to work around this problem and speed things up a bit even with the stock Gtk+ models: 

  1. You should detach your list store or tree store from the tree view before doing your mass insertions, then do your insertions, and only connect your store to the tree view again when you are done with your insertions. Save a pointer/reference to the model, detach the model from view with `gtk.TreeView.TreeView.setModel()` (passing nothing), add rows and then set the store back to the `model` with something like `gtk.TreeView.TreeView.setModel(model)`

  2.  Make sure that sorting is disabled while you are doing your mass insertions, otherwise your store might be resorted after each and every single row insertion, which is going to be everything but fast. 

  3. You should not keep around a lot of tree row references if you have so many rows, because with each insertion (or removal) every single tree row reference will check whether its path needs to be updated or not. 

---


### Basic Example:

A better approach is to wrap them into two classes (model and a view) and put them 
into their seperate module. Module should have an enum for columns, a model class and a 
view class.

```d
import gtk.ApplicationWindow : ApplicationWindow;
import gtk.Application : Application;
import gtk.HeaderBar : HeaderBar;
import gtk.ListStore : ListStore;
import gtkc.gtktypes : GType;
import gtk.TreeIter : TreeIter;
import gtk.TreeView : TreeView;

// module StorageLS
public enum Columns {
    id,
    deviceName,
    capacityGB,
    usedGB,
    usagePercentage
}

class StorageLS : ListStore {

public:
    this() {
        // uint id, in string deviceName, double capacityGB, double usedGB
        // construct a ListStore with corresponding GTypes
        // note that the last item in the array is for a derived item, usagePercentage
        super([GType.UINT, GType.STRING, GType.DOUBLE, GType.DOUBLE, GType.DOUBLE]);
    }

    void addStorage(uint id, in string deviceName, double capacityGB, double usedGB) {
        TreeIter iter = this.createIter(); // base ListStore.createIter()
        this.setValue(iter, Columns.id, id); // base ListStore.setValue()
        this.setValue(iter, Columns.deviceName, deviceName);
        this.setValue(iter, Columns.capacityGB, capacityGB);
        this.setValue(iter, Columns.usedGB, usedGB);
        this.setValue(iter, Columns.usagePercentage, usedGB * 100 / capacityGB);

        /* 
            returning the iter is not required because ListStore does not have children
            if it was a TreeStore, the returned iter would be necessary to add to child rows
        */
    }
}

class StorageTV : TreeView {
    import gtk.TreeViewColumn : TreeViewColumn;

private:
    TreeViewColumn idCol, deviceNameCol, capacityGBCol, usedGBCol, usedPercentageCol;
public:
    this(ref StorageLS storageListModel) {
        populate(storageListModel);
    }

    this() {

    }

    /*
        Why flush(), why populate()?
        
        Sometimes when you want to a big number of rows to the list store, 
        it's more efficient to detach the view from the modle with flush(). 
        you can always reconnect the view to the model using populate() again.
    */

    public void populate(ref StorageLS storageListModel) {
        import gtk.CellRendererText : CellRendererText;

        idCol = new TreeViewColumn("ID", new CellRendererText(), "text", Columns.id);
        deviceNameCol = new TreeViewColumn("Device", new CellRendererText(),
                "text", Columns.deviceName);
        capacityGBCol = new TreeViewColumn("Capacity (GB)",
                new CellRendererText(), "text", Columns.capacityGB);
        usedGBCol = new TreeViewColumn("Used space (GB)",
                new CellRendererText(), "text", Columns.usedGB);
        usedPercentageCol = new TreeViewColumn(`Percentage (%)`,
                new CellRendererText(), "text", Columns.usagePercentage);
        this.appendColumn(idCol);
        this.appendColumn(deviceNameCol);
        this.appendColumn(capacityGBCol);
        this.appendColumn(usedGBCol);
        this.appendColumn(usedPercentageCol);
        this.setModel(storageListModel);
    }

    public void flush() {
        idCol = null; 
        deviceNameCol =null;
        capacityGBCol = null; 
        usedGBCol = null; 
        usedPercentageCol = null;
        this.setModel(null);
    }
}

// module main
HeaderBar genHeaderBar(ref ApplicationWindow aw) {
    auto hb = new HeaderBar();
    hb.setTitle("GTKD MVC demo");
    hb.setSubtitle("Model");
    hb.setShowCloseButton(true);
    return hb;
}

void appMain(ref Application app, ref ApplicationWindow aw) {
    aw.setTitlebar(genHeaderBar(aw));

    auto storages = new StorageLS();
    storages.addStorage(1, "/dev/sda", 1021.66, 533.89);
    storages.addStorage(2, "/dev/sdb", 0.5, 0.11);
    storages.addStorage(3, "/dev/sdc", 15.87, 7.12);

    auto storagelist = new StorageTV(storages);

    import gtk.Box : Box;
    import gtkc.gtktypes : GtkOrientation;

    auto vbox = new Box(GtkOrientation.VERTICAL, 5);
    aw.add(vbox);
    scope (success)
        vbox.packStart(storagelist, true, true, 0);
}

int main(string[] args) {
    import gio.Application : GA = Application;
    import gtkc.gtktypes : GApplicationFlags;

    auto app = new Application("org.gitlab.gtkd.demo", GApplicationFlags.FLAGS_NONE);
    app.addOnActivate(delegate void(GA _) {

        auto aw = new ApplicationWindow(app);
        scope (success)
            aw.showAll();
        appMain(app, aw);
    });
    return app.run(args);
}
```

![](https://i.imgur.com/JfGUJWO.png)

# TODO:

- Iteratting

- Sorting and filtering

- Listening for events

- Introduce path and how to modify models with them

---

### Recomended reading:

- https://developer.gnome.org/gtk3/stable/TreeWidget.html

- https://sites.google.com/site/gtkdtutorial/#chapter6_1

- https://en.wikibooks.org/wiki/GTK%2B_By_Example/Tree_View/Tree_Models
