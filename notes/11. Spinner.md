# Spinner

A `gtk.Spinner.Spinner` widget displays an icon-size spinning animation. It is often used as an alternative to a Gtk ProgressBar for displaying indefinite activity, instead of actual progress.

To start the animation, use `gtk.Spinner.Spinner.start()`, to stop it use `gtk.Spinner.Spinner.stop()`.

### Example:

Let's create an app that let's users input a specified time in seconds and display a Gtk Spinner for the given time. The app should disable the input and any other buttons while the spinner is running and should display an ETA in the trigger button. The accuracy of the timing is not important for the sake of demonstration.

```d
int main(string[] args) {
    import gio.Application : GioApp = Application;
    import gtk.Application : Application;
    import gtkc.giotypes : GApplicationFlags;

    auto app = new Application("org.gitlab.radagast.spinner", GApplicationFlags.FLAGS_NONE);
    app.addOnActivate(delegate void(GioApp _) {
        import gtk.ApplicationWindow : ApplicationWindow;

        auto aw = new ApplicationWindow(app);
        scope (success)
            aw.showAll();

        aw.setDefaultSize(320, 200);
        aw.setBorderWidth(40);
        aw.setTitle("Spinner Demo");

        import gtk.Box : Box;
        import gtkc.gtktypes : GtkOrientation;

        auto vbox = new Box(GtkOrientation.VERTICAL, 10);
        scope (success)
            aw.add(vbox);

        import gtk.Spinner : Spinner;

        auto spinner = new Spinner();
        vbox.packStart(spinner, true, true, 0);

        auto hbox = new Box(GtkOrientation.HORIZONTAL, 10);
        scope (success)
            vbox.packStart(hbox, false, true, 0);

        import gtk.SpinButton : SpinButton;
        import gtkc.gtktypes : SpinButtonUpdatePolicy;

        auto spinButton = new SpinButton(0, 50, 1);
        spinButton.setNumeric(true);
        spinButton.setUpdatePolicy(SpinButtonUpdatePolicy.IF_VALID);
        hbox.packStart(spinButton, true, true, 0);

        import gtk.Button : Button;

        auto trigger = new Button("Start");
        trigger.addOnClicked(delegate void(Button _) {
            // retrieve the time
            auto time = spinButton.getValueAsInt();

            // set the button and the spinbutton insensitive
            spinButton.setSensitive(false);
            trigger.setSensitive(false);

            // start the spinner
            spinner.start();

            // stop the spinner after the given time
            import glib.Timeout : Timeout;  // read the foot-note
            import gtkc.gtktypes : GPriority;

            auto timeout = new Timeout(1000, delegate bool() {
                static int s;
                s += 1;
                if (s >= time) {
                    s = 0;
                    // stop the spinner
                    spinner.stop();
                    // make the button and the spinbutton sensitive again
                    spinButton.setSensitive(true);
                    trigger.setLabel("Start");
                    trigger.setSensitive(true);
                    return false;
                }
                else {
                    import std.format : format;

                    trigger.setLabel(format("Start (%s)", time - s));
                    return true;
                }
            }, GPriority.HIGH);
        });
        hbox.packStart(trigger, true, true, 0);
    });
    return app.run(args);
}
```
![Imgur image1](https://i.imgur.com/CMG12NR.png)
![Imgur image2](https://i.imgur.com/13wP56N.png)

---

> What is this `glib.Timeout.Timeout`?

Refer to the Glib Timeout section in the misc directory.