# Entry

Entry widgets allow the user to enter text. You can change the contents with the `gtk.Entry.Entry.setText()` method, and read the current contents with the `gtk.Entry.Entry.getText()` method. You can also limit the number of characters the Entry can take by calling `gtk.Entry.Entry.setMaxLength()`.

Occasionally you might want to make an Entry widget read-only. This can be done by passing False to the `gtk.Entry.Entry.setEditable()` method.

Entry widgets can also be used to retrieve passwords from the user. It is common practice to hide the characters typed into the entry to prevent revealing the password to a third party. Calling `gtk.Entry.Entry.setVisibility()` with False will cause the text to be hidden.

`gtk.Entry.Entry` has the ability to display progress or activity information behind the text. This is similar to `gtk.ProgressBar.Progressbar` widget and is commonly found in web browsers to indicate how much of a page download has been completed. To make an entry display such information, use `gtk.Entry.Entry.setProgressFraction()`, `gtk.Entry.Entry.setProgressPulseStep()`, or `gtk.Entry.Entry.progressPulse()`.

Additionally, an Entry can show icons at either side of the entry. These icons can be activatable by clicking, can be set up as drag source and can have tooltips.

### Example:

```d
int main(string[] args) {
    import gio.Application : GioApp = Application;
    import gtkc.giotypes : GApplicationFlags;
    import gtk.Application : Application;
    import gtk.ApplicationWindow : ApplicationWindow;

    auto app = new Application("org.gitlab.radagast.gtkdnote.textview",
            GApplicationFlags.FLAGS_NONE);
    app.addOnActivate(delegate void(GioApp _) {
        auto aw = new ApplicationWindow(app);
        scope (success)
            aw.showAll();

        aw.setTitlebar(() {
            import gtk.HeaderBar : HeaderBar;

            auto hb = new HeaderBar();
            hb.setTitle("Enter your password");
            hb.setSubtitle("Entry Demo");
            hb.setShowCloseButton(true);

            return hb;
        }());

        aw.add(() {
            import gtk.Box : Box;
            import gtkc.gtktypes : GtkOrientation;

            auto vbox = new Box(GtkOrientation.VERTICAL, 5);

            import gtk.Entry : Entry;

            auto entry = new Entry("Enter your password", 80);

            vbox.packStart(entry, true, true, 0);

            auto hbox = new Box(GtkOrientation.HORIZONTAL, 5);

            import gtk.CheckButton : CheckButton;
            import gtk.ToggleButton : ToggleButton;

            // The leading `_` is for mnemonics so optional
            auto checkEditable = new CheckButton("_Editable");
            checkEditable.setActive(true);

            /*
                IMPORTANT NOTE: The delegate below takes a ToggleButton
                as its parameter. `gtk.CheckButton` inherits from 
                `gtk.ToggleButton` so passing a CheckButton is allowed.
            */

            checkEditable.addOnToggled(delegate void(ToggleButton _) {
                static bool activate = false;
                entry.setEditable(activate);
                activate = !activate;
            });
            hbox.packStart(checkEditable, true, true, 0);

            auto checkVisible = new CheckButton("_Visible");
            checkVisible.setActive(true);
            checkVisible.addOnToggled(delegate void(ToggleButton _) {
                static bool visible = false;
                entry.setVisible(visible);
                visible = !visible;
            });
            hbox.packStart(checkVisible, true, true, 0);

            /*************
            auto checkPulse = new CheckButton("_Pulse");
            checkPulse.setActive(true);
            checkPulse.addOnToggled(delegate void(ToggleButton _) {
                if (checkPulse.getActive()) {
                    // Call the pulse delegate every 100ms
                    entry.setProgressPulseStep(0.2);
                    //?
                }
                else {

                }
            });
            hbox.packStart(checkPulse, true, true, 0);
            **************/

            auto checkHide = new CheckButton("Hide");
            checkHide.setActive(true);
            entry.setVisibility(!checkHide.getActive());
            checkHide.addOnToggled(delegate void(ToggleButton _) {
                static bool visible = false;
                entry.setVisibility(visible);
                visible = !visible;
            });
            hbox.packStart(checkHide, true, true, 0);

            hbox.packEnd(() {
                import gtk.Button : Button;

                return new Button("Close", delegate void(Button _) { app.quit(); });
            }(), true, true, 0);

            vbox.packStart(hbox, true, true, 0);
            return vbox;
        }());
    });
    return app.run(args);
}

```

![Imgur image](https://i.imgur.com/mM4rmE9.png)


----

### TODO:

* Try and port `ProgressPulse` Example. Help wanted, I'm stuck here.
  
  Relevant (https://forum.gtkd.org/groups/GtkD/thread/857/)