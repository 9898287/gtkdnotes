# Buttons

## Button

The Button widget is another commonly used widget. It is generally used to attach a function that is called when the button is pressed.

The `gtk.Button.Button` widget can hold any valid child widget. That is it can hold most any other standard `gtk.Widget.Widget`. The most commonly used child is the `gtk.Label`.

Usually, you want to connect to the button’s “clicked” signal with `addOnClicked()` which is emitted when the button has been pressed and released.

### Example:

```d
int main(string[] args) {
    import gio.Application : GioApp = Application;
    import gtk.Application : Application;
    import gtkc.giotypes : GApplicationFlags;

    auto app = new Application("org.gitlab.radagast.gtkd.buttons", GApplicationFlags.FLAGS_NONE);
    app.addOnActivate(delegate void(GioApp _) {
        import gtk.ApplicationWindow : ApplicationWindow;

        // add main window
        auto aw = new ApplicationWindow(app);
        scope (success)
            aw.showAll();

        // add headerbar
        aw.setTitlebar(() {
            import gtk.HeaderBar : HeaderBar;

            auto hb = new HeaderBar();
            hb.setTitle("Button");
            hb.setSubtitle("Demo");
            hb.setShowCloseButton(true);
            return hb;
        }()); // end add headerbar

        // add Box containing 3 buttons
        aw.add(() {
            import gtk.Box : Box;
            import gtkc.gtktypes : GtkOrientation;

            auto hbox = new Box(GtkOrientation.HORIZONTAL, 5);

            import gtk.Button : Button;
            import std.stdio;

            // add clickMe button
            hbox.packStart(new Button("Click Me!", delegate void(Button _) {
                writeln(`"Click Me!" was clicked.`);
            }), true, true, 0); // end add clickMe button

            // add open button
            hbox.packStart(() {
                auto btn = new Button();
                btn.setLabel("Open");
                btn.addOnClicked(delegate void(Button _) {
                    writeln(`"Open" was clicked.`);
                });
                return btn;
            }(), true, true, 0); // end add open button

            // add close button
            auto closeButton = new Button("Close", delegate void(Button _) {
                app.quit();
            });
            hbox.packStart(closeButton, true, true, 0);
            // end add close button

            return hbox;
        }()); // end add Box containing 3 buttons

    });
    return app.run(args);
}
```

![Imgur image1](https://i.imgur.com/iatZMix.png)

## ToggleButton

A `gtk.ToggleButton.ToggleButton` is very similar to a normal `gtk.Button.Button`, but when clicked they remain activated, or pressed, until clicked again. When the state of the button is changed, the “toggled” signal is emitted.

To retrieve the state of the `gtk.ToggleButton.ToggleButton`, you can use the `gtk.ToggleButton.ToggleButton.getActive()` method. This returns True if the button is “down”. You can also set the toggle button’s state, with `gtk.ToggleButton.ToggleButton.setActive()`. Note that, if you do this, and the state actually changes, it causes the “toggled” signal to be emitted.

```d
int main(string[] args) {
    import gio.Application : GioApp = Application;
    import gtk.Application : Application;
    import gtkc.giotypes : GApplicationFlags;

    auto app = new Application("org.gitlab.radagast.gtkd.buttons", GApplicationFlags.FLAGS_NONE);
    app.addOnActivate(delegate void(GioApp _) {
        import gtk.ApplicationWindow : ApplicationWindow;

        // add main window
        auto aw = new ApplicationWindow(app);
        scope (success)
            aw.showAll();

        // add headerbar
        aw.setTitlebar(() {
            import gtk.HeaderBar : HeaderBar;

            auto hb = new HeaderBar();
            hb.setTitle("ToggleButton");
            hb.setSubtitle("Demo");
            hb.setShowCloseButton(true);
            return hb;
        }()); // end add headerbar

        // add toggle button
        aw.add(() {
            import gtk.ToggleButton : ToggleButton;

            auto tb = new ToggleButton("Click me");
            tb.addOnToggled(delegate void(ToggleButton _) {
                import std.stdio : writefln;

                writefln("Button was toggled %s.", tb.getActive() ? "on" : "off");
            });
            return tb;
        }()); // end add toggle button
    });
    return app.run(args);
}
```
![Imgur Unclicked](https://i.imgur.com/ldIeEGi.png)
![Imgur Clicked](https://i.imgur.com/tAOwEoE.png)

## CheckButton

`gtk.CheckButton.CheckButton` inherits from `gtk.ToggleButton.ToggleButton`. The only real difference between the two is `gtk.CheckButton.CheckButton`’s appearance. A `gtk.CheckButton.CheckButton` places a discrete `gtk.ToggleButton.ToggleButton` next to a widget, (usually a `gtk.Label.Label`). The “toggled” signal, `gtk.ToggleButton.ToggleButton.setActive()` and `gtk.ToggleButton.ToggleButton.getActive()` are inherited.

## RadioButton

Like checkboxes, radio buttons also inherit from `gtk.ToggleButton.ToggleButton`, but these work in groups, and only one `gtk.RadioButton.RadioButton` in a group can be selected at any one time. Therefore, a `gtk.RadioButton.RadioButton` is one way of giving the user a choice from many options. You can get the underlying singly linked list of radio buttons with `getGroup()` method, join radio buttons with `joinGroup()` method and set group with `setGroup()` method.

You can create radio buttons from three constructors:

1. `RadioButton(GtkRadioButton* gtkRadioButton, bool ownedRef = false)` 

   Create a radio button from an existing one. Use this to mimic the properties of the pre-existing radio button.

2. `RadioButton(RadioButton radioButton0, string label, bool mnemonic = true)`

   Use this to create and connect a radio button to the same group as `radioButton0`.

3. `RadioButton(string label, bool mnemonic = true)`

    Use this to create an original radio button.

### Example:

Let's create a group of 3 radio buttons:

```d
int main(string[] args) {
    import gio.Application : GioApp = Application;
    import gtk.Application : Application;
    import gtkc.giotypes : GApplicationFlags;

    auto app = new Application("org.gitlab.radagast.gtkd.buttons", GApplicationFlags.FLAGS_NONE);
    app.addOnActivate(delegate void(GioApp _) {
        import gtk.ApplicationWindow : ApplicationWindow;

        // add main window
        auto aw = new ApplicationWindow(app);
        scope (success)
            aw.showAll();

        // add headerbar
        aw.setTitlebar(() {
            import gtk.HeaderBar : HeaderBar;

            auto hb = new HeaderBar();
            hb.setTitle("Select an option");
            hb.setSubtitle("RadioButton Demo");
            hb.setShowCloseButton(true);
            return hb;
        }()); // end add headerbar

        aw.add(() {
            import gtk.Box : Box;
            import gtkc.gtktypes : GtkOrientation;

            auto hbox = new Box(GtkOrientation.HORIZONTAL, 5);

            import gtk.RadioButton : RadioButton;

            auto option1 = new RadioButton("Option 1");
            hbox.packStart(option1, true, true, 0);

            auto option2 = new RadioButton(option1, "Option 2");
            hbox.packStart(option2, true, true, 0);

            auto option3 = new RadioButton(option1);
            option3.setLabel("Option 3");
            hbox.packStart(option3, true, true, 0);

            return hbox;
        }());

    });
    return app.run(args);
}
```

![Imgur image](https://i.imgur.com/0Ygyds7.png)

## LinkButtons:

A `gtk.LinkButton.LinkButton` is a `gtk.Button.Button` with a hyperlink, similar to the one used by web browsers, which triggers an action when clicked. It is useful to show quick links to resources.

The URI bound to a `gtk.LinkButton.LinkButton` can be set specifically using `gtk.LinkButton.LinkButton.setUri()`, and retrieved using `gtk.LinkButton.LinkButton.getUri()`.

### Example:

```d
void main(string[] args) {
    import gtk.Main : Main;
    import gtk.MainWindow : MainWindow;

    Main.init(args);
    scope (success)
        Main.run();

    auto mw = new MainWindow("LinkButton Demo");
    scope (success)
        mw.showAll();

    mw.setSizeRequest(500, 100);

    mw.add(() {
        import gtk.LinkButton : LinkButton;

        return new LinkButton(`http://www.gtk.org`, "Visit GTK+ Homepage");
    }());
}


```
![Imgur image](https://i.imgur.com/VQUXffO.png)

## SpinButton

A `gtk.SpinButton.SpinButton` is an ideal way to allow the user to set the value of some attribute. Rather than having to directly type a number into a `gtk.Entry.Entry`, `gtk.SpinButton.SpinButton` allows the user to click on one of two arrows to increment or decrement the displayed value. A value can still be typed in, with the bonus that it can be checked to ensure it is in a given range. The main properties of a `gtk.SpinButton.SpinButton` are set through `gtk.Adjustment.Adjustment`.

To change the value that `gtk.SpinButton.SpinButton` is showing, use `gtk.SpinButton.SpinButton.setValue()`. The value entered can either be an integer or float, depending on your requirements, use `gtk.SpinButton.SpinButton.getValue()` or `gtk.SpinButton.SpinButton.getValueAsInt()`, respectively.

When you allow the displaying of float values in the spin button, you may wish to adjust the number of decimal spaces displayed by calling `gtk.SpinButton.SpinButton.setDigits()`.

By default, `gtk.SpinButton.SpinButton` accepts textual data. If you wish to limit this to numerical values only, call `gtk.SpinButton.SpinButton.setNumeric()` with True as argument.

We can also adjust the update policy of `gtk.SpinButton.SpinButton`. There are two options here; by default the spin button updates the value even if the data entered is invalid. Alternatively, we can set the policy to only update when the value entered is valid by calling `gtk.SpinButton.SpinButton.setUpdatePolicy()`.

### Example:

```d
int main(string[] args) {
    import gio.Application : GioApp = Application;
    import gtk.Application : Application;
    import gtkc.giotypes : GApplicationFlags;

    auto app = new Application("org.gitlab.radagast.gtkdnotes.spinsbutton",
            GApplicationFlags.FLAGS_NONE);
    app.addOnActivate(delegate void(GioApp _) {
        import gtk.ApplicationWindow : ApplicationWindow;

        auto aw = new ApplicationWindow(app);
        scope (success)
            aw.showAll();
        aw.setDefaultSize(400, 40);

        aw.setTitlebar(() {
            import gtk.HeaderBar : HeaderBar;

            auto hb = new HeaderBar();
            hb.setTitle("SpinButton Demo");
            hb.setShowCloseButton(true);
            return hb;
        }());

        aw.add(() {
            import gtk.Box : Box;
            import gtkc.gtktypes : GtkOrientation;

            auto hbox = new Box(GtkOrientation.HORIZONTAL, 10);

            import gtk.SpinButton : SpinButton;

            auto spinButton = new SpinButton(0, 100, 1);
            hbox.packStart(spinButton, true, true, 0);

            import gtk.CheckButton : CheckButton;
            import gtk.ToggleButton : ToggleButton;

            auto numericCheck = new CheckButton("Set numeric");
            numericCheck.setActive(true);
            numericCheck.addOnToggled(delegate void(ToggleButton _) {
                spinButton.setNumeric(numericCheck.getActive());
            });
            hbox.packStart(numericCheck, true, true, 0);

            auto validityCheck = new CheckButton("Valid only");
            validityCheck.addOnToggled(delegate void(ToggleButton _) {
                import gtkc.gtktypes : SpinButtonUpdatePolicy;

                spinButton.setUpdatePolicy(validityCheck.getActive()
                ? SpinButtonUpdatePolicy.IF_VALID : SpinButtonUpdatePolicy.ALWAYS);
            });
            hbox.packStart(validityCheck, true, true, 0);

            return hbox;
        }());
    });
    return app.run(args);
}
```

![Imgur image](https://i.imgur.com/9Js5XBx.png)

## Switch

A `gtk.Switch.Switch` is a widget that has two states: on or off. The user can control which state should be active by clicking the empty area, or by dragging the handle.

**_You shouldn’t use the “activate” signal on the `gtk.Switch.Switch`_** which is an action signal and emitting it causes the switch to animate. Applications should never connect to this signal, but use the “notify::active” signal.

### Example:

```d
void main(string[] args) {
    import gtk.Main : Main;
    import gtk.MainWindow : MainWindow;

    Main.init(args);
    scope (success)
        Main.run();

    auto mw = new MainWindow("Switch Demo");
    scope (success)
        mw.showAll();

    mw.add(() {
        import gtk.Box : Box;
        import gtkc.gtktypes : GtkOrientation;

        auto vbox = new Box(GtkOrientation.VERTICAL, 5);
        vbox.setBorderWidth(40);

        import gtk.Switch : Switch;

        auto sw = new Switch();
        // There are two ways I usually handle switches.

        // 1. Terse:
        // With the "active" at the end, the delegate is only called 
        // when the "active" property switch changes.
        sw.addOnNotify(delegate void(_, __) {
            import std.stdio : writeln;

            writeln(sw.getActive() ? "On" : "Off");
        }, "active");

        //2. Verbose:
        // import gobject.ParamSpec : ParamSpec;
        // import gobject.ObjectG : ObjectG;
        // sw.addOnNotify(delegate void(ParamSpec p, ObjectG _y) {
        //     if (p.getName() == "active") {
        //         static bool on;
        //         on = sw.getActive();

        //         //do stuf
        //         import std.stdio : writeln;

        //         writeln(on ? "On" : "Off");
        //     }
        // });

        vbox.packStart(sw, true, true, 0);
        return vbox;
    }());
}
```

![Imgur Image](https://i.imgur.com/3YbjdMI.png)

