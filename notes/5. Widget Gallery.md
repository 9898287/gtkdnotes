Gtk provides users with a wide range of widgets. You can find them in https://developer.gnome.org/gtk3/stable/ch03.html

Furthermore, if you have `libgtk3` or alike installed you can test them yourself by simply issuing `gtk3-widget-factory` in your desktop.

![](https://i.imgur.com/w0khmOn.png)