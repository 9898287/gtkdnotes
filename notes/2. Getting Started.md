## Simple Example

This program will create an empty 200 x 200 pixel window. 

## gtk-3 (encouraged) style

```d
import gio.Application : GioApplication = Application;
import gtk.Application;
import gtk.ApplicationWindow;

int main(string[] args) {
    auto application = new Application(
                            "org.gtkd.demo.helloworld", 
                            GApplicationFlags.FLAGS_NONE
                       );        
    application.addOnActivate(delegate void(GioApplication app) {
        auto appWindow = new ApplicationWindow(application);
        appWindow.setSizeRequest(200, 200);
        appWindow.showAll();
    });
    return application.run(args);
}
```

Confused about the modules and where they came from? 
Here is the same app with fully qualified names:

```d
static import gio.Application;
static import gtk.Application;
static import gtk.ApplicationWindow;

int main(string[] args) {
    auto application = new gtk.Application.Application("org.gtkd.demo.helloworld",
            gtk.ApplicationWindow.GApplicationFlags.FLAGS_NONE);
    application.addOnActivate(delegate void(gio.Application.Application _) {
        auto appWindow = new gtk.ApplicationWindow.ApplicationWindow(application);
        appWindow.setSizeRequest(200, 200);
        appWindow.showAll();
    });
    return application.run(args);
}
```

In a GTK+ application, the purpose of the main() function is to create a `gtk.Application.Application`  object and run it. In this example a `gtk.Application.Application` named `application` is called and then initialized using a constructor.

When creating a `gtk.Application.Application` you need to pick an application identifier (a name) and input to the constructor stated above as parameter. For this example `org.gtkd.demo.helloworld` is used but for choosing an identifier for your application see [this guide](https://wiki.gnome.org/HowDoI/ChooseApplicationID). Lastly constructor takes a `GApplicationFlags` as input for your application, if your application would have special needs. 

Next the activate signal is connected to the delegate above. The activate signal will be sent when your application is launched with `gtk.Application.run()`.  The `gtk.Application.run()` also takes as arguments the pointers to the command line arguments counter and string array; this allows GTK+ to parse specific command line arguments that control the behavior of GTK+ itself. The parsed arguments will be removed from the array, leaving the unrecognized ones for your application to parse.

Note that `widget.addOnConnect()` is GtkD's equivalent of `g_signal_connect("activate", ...)` functions.

In the delegate an `gtk.ApplicationWindow.ApplicationWindow` is created and the `showAll()` method ensures that the window, along with all it's widgets is displayed.

## gtk-2 style

```d
import gtk.Main;
import gtk.MainWindow;

void main(string[] args) {
    Main.init(args);
    auto appWindow = new MainWindow("Demo title");
    appWindow.setDefaultSize(200, 200);
    appWindow.showAll();
    Main.run();
}
```

Or, if you don't want to use MainWindow:

```d
import gtk.Main, gtk.Window, gtk.Widget;

void main(string[] args) {
	Main.init(args);
	auto window = new Window("Title");
	window.setDefaultSize(200, 200);
	window.addOnDestroy(delegate void(Widget _) { Main.quit(); });
	window.showAll();
	Main.run();
}
```

## Screenshot


![window](https://i.imgur.com/OvAiI3W.png)

## Extended Example

Let's add a button to a window

```d
void main(string[] args) {
	import gtk.Main : Main;

	Main.init(args);
	import gtk.MainWindow : MainWindow;

	auto window = new MainWindow("Title");
	window.setDefaultSize(200, 200);
	import gtk.Button : Button;

	auto button = new Button("Click here");
	button.addOnClicked(delegate void(Button _) {
		import std.stdio : writefln;

		static ulong count = 1;
		writefln("Button was clicked %s time%s.", count, count == 1 ? "" : "s");
		++count;
	});
	window.add(button);
	window.showAll();
	Main.run();
}
```
![Imgur screenshot](https://i.imgur.com/IsohsXI.png)