# Layout Containers

## Box Layout
While many GUI toolkits require you to precisely place widgets in a window, using absolute positioning, GTK+ uses a different approach. Rather than specifying the position and size of each widget in the window, you can arrange your widgets in rows, columns, and/or tables. The size of your window can be determined automatically, based on the sizes of the widgets it contains. And the sizes of the widgets are, in turn, determined by the amount of text they contain, or the minimum and maximum sizes that you specify, and/or how you have requested that the available space should be shared between sets of widgets. You can perfect your layout by specifying padding distance and centering values for each of your widgets. GTK+ then uses all this information to resize and reposition everything sensibly and smoothly when the user manipulates the window.

GTK+ arranges widgets hierarchically, using containers. They are invisible to the end user and are inserted into a window, or placed within each other to layout components. There are two flavours of containers: single-child containers, which are all descendants of Gtk.Bin, and multiple-child containers, which are descendants of `gtk.Container.Container`. The most commonly used are vertical or horizontal boxes (`gtk.Box.Box`) and grids (`gtk.Grid.Grid`).

## Box Layout Example:

```d
void main(string[] args) {
	import gtk.Main : Main;

	Main.init(args);
	import gtk.MainWindow : MainWindow;

	auto window = new MainWindow("Hello world");
	import gtk.Box : Box;
	import gtkc.gtktypes : GtkOrientation;

	auto box = new Box(GtkOrientation.HORIZONTAL, /*spacing:*/ 10);

	import gtk.Button : Button;

	auto sayhelloButton = new Button("Say Hello");
	sayhelloButton.addOnClicked(delegate void(Button _) {
		import std.stdio : writeln;

		writeln("Hello");
	});
	// packStart method positons widgets (like buttons) from left to right
	// packEnd does the opposite
	// void packStart (Widget child, bool expand, bool fill, uint padding); 
	box.packStart(sayhelloButton, true, true, 3);
	auto goodbyeButton = new Button("Goodbye");
	goodbyeButton.addOnClicked(delegate void(Button _) { Main.quit(); });
	box.packStart(goodbyeButton, true, true, 3);
	window.add(box);
	window.showAll();
	Main.run();
}
```

![Imgur screenshot](https://i.imgur.com/eX6gbT4.png)

## Grid Layout

### Section 1:
Grid Layout `gtk.Grid.Grid` is a container which arranges its child widgets in rows and columns, but you do not need to specify the dimensions in the constructor. 

`gtk.Grid.Grid.attach()` lets you attach widgets to a grid.
The this method takes five parameters:

1. The child parameter is the `Gtk.Widget` to add.
2. `left` is the column number to attach the left side of `child` to.
3. `top` indicates the row number to attach the top side of `child` to.
4. `width` and `height` indicate the number of columns that the `child` will span, 
and the number of rows that the `child` will span, respectively.


### Section 2:
It is also possible to add a child next to an existing child, using `gtk.Grid.Grid.attachNextTo()`, 
which also takes five parameters:

1.  Widget child,
2.  Widget sibling,
3.  GtkPositionType side,
4.  int width,
5.  int height

The widget is placed next to sibling , on the side determined by side. 
When sibling is NULL, the widget is placed in row (for left or right placement) 
or column 0 (for top or bottom placement), at the end indicated by side.

```d
void main(string[] args) {
    import gtk.Main : Main;
    import gtk.MainWindow : MainWindow;

    Main.init(args);
    auto stage = new MainWindow("Grid Layout");
    import gtk.Grid : Grid;

    auto grid = new Grid();

    stage.add(grid);
    import gtk.Button : Button;

    // Gives a new button, for the purpose of demonstration
    Button makeAButton() {
        static ulong buttonNumber = 0;
        import std.format : format;

        return new Button(format("Button %s", ++buttonNumber));
    }

    // Read Section 1.
    grid.attach(makeAButton(), 0, 0, 2, 1);
    grid.attach(makeAButton(), 0, 1, 1, 1);
    auto button3 = makeAButton();
    grid.attach(button3, 1, 1, 1, 1);

    // Read Section 2.
    import gtkc.gtktypes : GtkPositionType;

    grid.attachNextTo(makeAButton(), button3, GtkPositionType.BOTTOM, 1, 1);
    // Read Section 3 (optional)
    
    // You can also use the gtk_container_add wrapper to add widgets horizontally
    import gtkc.gtktypes : GtkOrientation;

    grid.add(makeAButton());
    stage.showAll();
    Main.run();
}
```

### Section 3:
Note that even if you passed `2` in the 4th parameter in the above function call, 
it would still take 1 cell. 
These cells only grow left and bottom.


![Imgur screenshot](https://i.imgur.com/xCUad4E.png)

## ListBox

A `gtk.ListBox.ListBox` is a **vertical** container that contains `gtk.ListBoxRow.ListBoxRow` children. These **rows can be dynamically sorted and filtered, and headers can be added dynamically depending on the row content**. It also allows keyboard and mouse navigation and selection like a typical list.

Using `gtk.ListBox.ListBox` is often an alternative to `gtk.TreeView.TreeView`, especially when the list content has a more complicated layout than what is allowed by a `gtk.CellRenderer.CellRenderer`, or when the content is interactive (i.e. has a button in it).

A `ListBox` expects you to add any number of `ListBoxRow`s.

Although a `gtk.ListBox.ListBox` must have only gtkListBoxRow children, you can add any kind of widget to it via `add()` and a `gtk.ListBoxRow.ListBoxRow` widget will automatically be inserted between the list and the widget.

### Basic example:

```d
void main(string[] args) {
    import gtk.Main : Main;
    import gtk.MainWindow : MainWindow;

    Main.init(args);
    scope (success)
        Main.run();

    auto mainWindow = new MainWindow("ListBox Demo");
    scope (success)
        mainWindow.showAll();

    import gtk.Box : Box;
    import gtkc.gtktypes : GtkOrientation;

    auto mainBox = new Box(GtkOrientation.VERTICAL, 0);
    scope (success)
        mainWindow.add(mainBox);

    import gtk.ListBox : ListBox;
    import gtk.ListBoxRow : ListBoxRow;
    import gtk.Button : Button;
    import gtkc.gtktypes : GtkSelectionMode;

    auto listBox = new ListBox();
    scope (success) {
        listBox.showAll();
        mainBox.add(listBox);
    }
    // Make these listBoxRows below unselectable
    listBox.setSelectionMode(GtkSelectionMode.NONE);
    // ListBoxRows are to be added to the ListBox
    auto listBoxRow = new ListBoxRow();

    // gtk.ListBox is a vertical container.

    import std.array : split;

    foreach (buttonName; "This is a listbox container".split())
        listBox.add(() {
            // this lambda:
            // 1. creates a new LixtBoxRow
            // 2. attaches a new button with the button name
            // 3. returns it
            auto currentListRow = new ListBoxRow();
            currentListRow.add(new Button(buttonName));
            return currentListRow;
        }());

    listBox.showAll();
}
```

![Imgur screenshot](https://i.imgur.com/GvXXQJN.png)

### A bit more elaborate example: 

(Note that the design below is not very suitable for real use, it's just) an 
example of how you can add boxes to the ListbBoxRows.)

```d
void main(string[] args) {
    import gtk.Main : Main;
    import gtk.MainWindow : MainWindow;

    Main.init(args);
    scope (success)
        Main.run();

    auto mainWindow = new MainWindow("ListBox Demo");
    scope (success)
        mainWindow.showAll();

    mainWindow.add(() {
        import gtk.ListBox : ListBox;
        import gtk.ListBoxRow : ListBoxRow;
        import gtkc.gtktypes : SelectionMode;

        auto mainList = new ListBox();
        mainList.setSelectionMode(SelectionMode.NONE);

        import gtk.Box : Box;
        import gtkc.gtktypes : GtkOrientation;
        import gtk.Label : Label;

        mainList.add(() {
            // automatic date time row
            auto hbox = new Box(GtkOrientation.HORIZONTAL, 5);
            hbox.packStart(() {
                // left row (lables)
                auto vbox = new Box(GtkOrientation.VERTICAL, 0);
                vbox.packStart(new Label("Automatic Date & Time"), true, true, 0);
                vbox.packStart(new Label("(Requires internet access)"), true, true, 0);
                return vbox;
            }(), true, true, 0);

            hbox.packStart(() {
                // right row (switch)
                import gtk.Switch : Switch;

                return new Switch();
            }(), false, false, 0);
            return hbox;
        }());

        mainList.add(() {
            // enable automatic update row
            auto eaurow = new ListBoxRow();
            eaurow.add(() {
                auto box = new Box(GtkOrientation.HORIZONTAL, 5);
                box.packStart(() {
                    auto hbox = new Box(GtkOrientation.HORIZONTAL, 5);
                    hbox.packStart(new Label("Enable Automatic Update"), true, true, 0);
                    hbox.packStart(() {
                        import gtk.CheckButton : CheckButton;

                        return new CheckButton();
                    }(), false, false, 0);
                    return hbox;
                }(), true, true, 0);
                return box;
            }());
            return eaurow;
        }());

        mainList.add(() {
            // date format row
            auto box = new Box(GtkOrientation.HORIZONTAL, 5);
            box.packStart(() {
                auto hbox = new Box(GtkOrientation.HORIZONTAL, 5);
                hbox.packStart(new Label("Date format"), true, false, 0);
                hbox.packStart(() {
                    import gtk.ComboBoxText : ComboBoxText;

                    auto comboBox = new ComboBoxText();
                    comboBox.insert(0, "0", "AM/PM");
                    comboBox.insert(1, "0", "24h");
                    return comboBox;
                }(), false, false, 0);
                return hbox;
            }(), true, true, 0);
            return box;
        }());

        mainList.showAll();
        return mainList;
    }());
}
```

![Imgur screenshot](https://i.imgur.com/VG8AW1S.png)

## Stacks and StackSwitches

A `gtk.Stack.Stack` is a container which only shows one of its children at a time. In contrast to `gtk.Notebook.Notebook`, `gtk.Stack.Stack` does not provide a means for users to change the visible child. Instead, the `gtk.StackSwitcher.StackSwitcher` widget can be used with `gtk.Stack.Stack` to provide this functionality.

Transitions between pages can be animated as slides or fades. This can be controlled with `gtk.Stack.Stack.setTransitionType()`. These animations respect the “gtk-enable-animations” setting.

Transition speed can be adjusted with `gtk.Stack.Stack.setTransitionDuration()`

The `gtk.StackSwitcher.StackSwitcher` widget acts as a controller for a `gtk.Stack.Stack`; it shows a row of buttons to switch between the various pages of the associated stack widget.

All the content for the buttons comes from the child properties of the `gtk.Stack.Stack`.

It is possible to associate multiple `gtk.StackSwitcher.StackSwitcher` widgets with the same `gtk.Stack.Stack` widget.

```d
void main(string[] args) {
    import gtk.Main : Main;

    Main.init(args);
    scope (success)
        Main.run();

    import gtk.MainWindow : MainWindow;

    auto mainWindow = new MainWindow("Stack Demo");

    scope (success)
        mainWindow.showAll();

    mainWindow.add(() {
        import gtk.Box : Box;
        import gtkc.gtktypes : GtkOrientation;

        auto vbox = new Box(GtkOrientation.VERTICAL, 0);

        import gtk.Stack : Stack;
        import gtk.StackSwitcher : StackSwitcher;

        auto stack = () {

            import gtkc.gtktypes : StackTransitionType;

            auto stack = new Stack();
            stack.setTransitionType(StackTransitionType.SLIDE_LEFT_RIGHT);
            stack.setTransitionDuration(200);

            stack.addTitled(() {
                import gtk.CheckButton : CheckButton;

                return new CheckButton("Click me!");
            }(), "name_check", "Check Button");

            stack.addTitled(() {
                import gtk.Label : Label;

                return new Label("A big fancy label");
            }(), "label", "A Label");
            return stack;
        }();
        vbox.packStart(stack, true, true, 0);
        auto stackSwitcher = () {
            auto ss = new StackSwitcher();
            ss.setStack(stack);
            return ss;
        }();
        vbox.packStart(stackSwitcher, true, true, 0);
        return vbox;
    }());
}
```

![Imgur image1](https://i.imgur.com/kEoUmXX.png)
![Imgur image2](https://i.imgur.com/XFABuvv.png)

## HeaderBar

A `gtk.HeaderBar.HeaderBar` is similar to a horizontal `gtk.Box.Box`, it allows to place children at the start or the end. In addition, it allows a title to be displayed. The title will be centered with respect to the width of the box, even if the children at either side take up different amounts of space.

Since GTK+ now supports Client Side Decoration, a `gtk.HeaderBar.HeaderBar` can be used in place of the title bar (which is rendered by the Window Manager).

A `gtk.HeaderBar.HeaderBar` is usually located across the top of a window and should contain commonly used controls which affect the content below. They also provide access to window controls, including the close window button and window menu.

### Example:

```d
void main(string[] args) {
    import gtk.Main : Main;
    import gtk.MainWindow : MainWindow;

    Main.init(args);
    scope (success)
        Main.run();
    auto mw = new MainWindow("This title will be overwritten");
    scope (success)
        mw.showAll();
    mw.setDefaultSize(300, 300);
    mw.setTitlebar(() {
        import gtk.HeaderBar : HeaderBar;

        auto hb = new HeaderBar();
        hb.setTitle("Your App");
        hb.setSubtitle("A Subtitle");

        hb.packEnd(() {
            import gtk.Button : Button;
            
            // instead of adding this button you could simply add
            // hb.showCloseButton(true);
            auto closeButton = new Button("Close");
            closeButton.addOnClicked(delegate void(Button _) { Main.quit(); });
            return closeButton;
        }());
        return hb;
    }());
}
```

![Imgur image](https://i.imgur.com/kH195q1.png)

## FlowBox

A `gtk.FlowBox.FlowBox` is a container that positions child widgets in sequence according to its orientation.

For instance, with the horizontal orientation, the widgets will be arranged from left to right, starting a new row under the previous row when necessary. Reducing the width in this case will require more rows, so a larger height will be requested.

Likewise, with the vertical orientation, the widgets will be arranged from top to bottom, starting a new column to the right when necessary. Reducing the height will require more columns, so a larger width will be requested.

The children of a `gtk.FlowBox.FlowBox` can be dynamically sorted and filtered.

Although a **`gtk.FlowBox.FlowBox` must have only `gtk.FlowBoxChild.FlowBoxChild`** children, you can add any kind of widget to it via `gtk.Container.Container.add()`, and a `gtk.FlowBoxChild.FlowBoxChild` widget will automatically be inserted between the box and the widget.


### Example:

```d
void main(string[] args) {
    import gtk.Main : Main;
    import gtk.Window : Window;

    Main.init(args);
    scope (success)
        Main.run();
    auto mw = new Window("This title will be overwritten");
    scope (success)
        mw.showAll();

    import gtk.Widget : Widget;

    mw.addOnDestroy(delegate void(Widget _) { Main.quit(); });
    mw.setDefaultSize(1080, 720);

    mw.setTitlebar(() {
        import gtk.HeaderBar : HeaderBar;

        auto hb = new HeaderBar();
        hb.setTitle("Your App");
        hb.setSubtitle("A Subtitle");
        hb.setShowCloseButton(true);

        import gtk.Button : Button;

        hb.packEnd(new Button("A Button"));

        hb.packStart(() {
            import gtk.Label : Label;
            import std.file : getcwd;

            return new Label(getcwd());
        }());
        return hb;
    }());

    // Add a ScrolledWindow
    mw.add(() {
        import gtk.ScrolledWindow : ScrolledWindow;
        import gtkc.gtktypes : PolicyType;

        // ctor ScrolledWindow(horizontalPolicy, verticalPolicy)
        auto sw = new ScrolledWindow(PolicyType.NEVER, PolicyType.AUTOMATIC);
        sw.add(() {
            // Add a Flowbox
            import gtk.FlowBox : FlowBox;
            import gtkc.gtktypes : Align;
            import gtkc.gtktypes : SelectionMode;

            auto fb = new FlowBox();
            fb.setValign(Align.START);
            fb.setMaxChildrenPerLine(30);
            fb.setSelectionMode(SelectionMode.NONE);

            // add items to flowbox
            import std.file : dirEntries, SpanMode, getcwd;

            foreach (string name; dirEntries(getcwd(), SpanMode.shallow)) {
                fb.add(() {
                    import gtk.Label : Label;
                    import std.path : baseName;

                    auto lbl = new Label(baseName(name));

                    return lbl;
                }());
            }
            return fb;
        }());
        return sw;
    }());
}
```

![Imgur image](https://i.imgur.com/PlOdIPR.png)

## Notebook

The `gtk.Notebook.Notebook` widget is a `gtk.Container.Container` whose children are pages that can be switched between using tab labels along one edge.

There are many configuration options for `gtk.Notebook.Notebook`. Among other things, you can choose on which edge the tabs appear (see `gtk.Notebook.Notebook.setTabPos()`), whether, if there are too many tabs to fit the notebook should be made bigger or scrolling arrows added (see `gtk.Notebook.Notebook.setScrollable()`, and whether there will be a popup menu allowing the users to switch pages (see `gtk.Notebook.Notebook.popupEnable()`, `gtk.Notebook.popupDisable()`).

### Example:

```d
import gio.Application : GioApp = Application;
import gtk.Application : Application;
import gtk.ApplicationWindow : ApplicationWindow, GApplicationFlags;

int main(string[] args) {
    auto application = new Application("org.gtkdnotes.demo.notebook", GApplicationFlags.FLAGS_NONE);
    application.addOnActivate(delegate void(GioApp _) {
        auto aw = new ApplicationWindow(application);
        scope (success)
            aw.showAll();

        aw.setTitlebar(() {
            import gtk.HeaderBar : HeaderBar;

            auto hb = new HeaderBar();
            hb.setTitle("Notebook Demo");
            hb.setShowCloseButton(true);
            return hb;
        }());

        aw.add(() {
            import gtk.Notebook : Notebook;

            auto nb = new Notebook();

            import gtk.Label : Label;
            import gtk.Box : Box;
            import gtkc.gtktypes : GtkOrientation;

            nb.appendPage(() {
                auto vb = new Box(GtkOrientation.VERTICAL, 0);
                vb.packStart(new Label("Welcome!"), true, true, 0);
                return vb;
            }(), new Label("Welcome page"));

            nb.appendPage(() {
                auto vb = new Box(GtkOrientation.VERTICAL, 0);
                vb.packStart(new Label("Page 2"), true, true, 0);
                return vb;
            }(), new Label("Page 2"));

            return nb;
        }());
    });
    return application.run(args);
}
```

![Imgur image1](https://i.imgur.com/xFB8wu6.png)
![Imgur image2](https://i.imgur.com/SS2GXaL.png)

Note that the `appendPage()` method appends pages and assigns page number to them. Page number starts from 0. 
If you want to add a callback that invokes an action everytime a page is selected you can use the 
`addOnSwitchPage()` method as below:

```d
nb.addOnSwitchPage(delegate void(Widget page, uint pagenum, Notebook _n) {
            if (pagenum == 1)
            {
                    import std.stdio: writeln;
                    writeln("Reached page: ", pagenum);
                }
            }
        });
```

